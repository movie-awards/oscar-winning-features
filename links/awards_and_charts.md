# Movie data #

## Choice of annual movie awards and movie charts ##

Oscar: Best Movie (since 1929)
https://en.wikipedia.org/wiki/Academy_Award_for_Best_Picture

Cannes: Goldene Palme (since 1939):
https://en.wikipedia.org/wiki/Palme_d%27Or

Box Office Hits/Highest Grossing Movie (since 1915)
https://en.wikipedia.org/wiki/List_of_highest-grossing_films 

People‘s Choice Award (since 1975)
*siehe people_choice_awards_wiki_links.txt*

## List of other annual movie awards and movie charts ##

Golden Globe: Best Film Drama (since 1944):
https://en.wikipedia.org/wiki/Golden_Globe_Award_for_Best_Motion_Picture_–_Drama

Golden Globe: Best Film: Komödie (since 1952):
https://en.wikipedia.org/wiki/Golden_Globe_Award_for_Best_Motion_Picture_–_Musical_or_Comedy

Locarno: Goldener Leopard (since 1946)
https://en.wikipedia.org/wiki/Golden_Leopard

Montreal: Grand Prix das Americas (since 1978)
https://en.wikipedia.org/wiki/Montreal_World_Film_Festival