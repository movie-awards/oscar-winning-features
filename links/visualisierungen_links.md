### Link zum Google-Doc mit Visualisierungsmöglichkeiten: 

https://docs.google.com/document/d/1xJUstl_XG3yWD3vkcJKji7ZusMXt07eI8gruL4C53oU/edit

### Links zu Visualisierungen

**WordCloud** (1 prize: plot):

https://www.jqueryscript.net/text/Word-Tag-Cloud-Generator-jQWCloud.html

or

https://www.jasondavies.com/wordcloud/

**Bar Charts** (multiple prizes: country, language, genre):

https://developers.google.com/chart/interactive/docs/gallery/barchart

Column Bars:

https://developers.google.com/chart/interactive/docs/gallery/columnchart 

Bubble Chart for correlations:

https://developers.google.com/chart/interactive/docs/gallery/bubblechart

Combo Charts:

https://developers.google.com/chart/interactive/docs/gallery/combochart 

Diff Charts

https://developers.google.com/chart/interactive/docs/gallery/diffchart

**Pie Charts** (1 prize: country, language, genre, semantic analysis)

https://developers.google.com/chart/interactive/docs/gallery/piechart#donut 

Histograms:

https://developers.google.com/chart/interactive/docs/gallery/histogram 

Stepped Area Chart:

https://developers.google.com/chart/interactive/docs/gallery/steppedareachart 

**Interactive table** (multiple prizes: most of the information: title, year, etc.):

https://developers.google.com/chart/interactive/docs/gallery/table 

Tree Map Charts:

https://developers.google.com/chart/interactive/docs/gallery/treemap 

**Timeline** (multiple prizes: most of the information: wiki-link, historical events, short plot, poster etc.):

https://timeline.knightlab.com/

**D3 Bubble Chart** (1 prize: topic modeling)

https://beta.observablehq.com/@mbostock/d3-bubble-chart

**Disjoint Force-Directed Graph** (1 prize: film title plus: actors, producers, directors, cinematographers, production companies)

https://beta.observablehq.com/@mbostock/disjoint-force-directed-graph
