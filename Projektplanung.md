# Projekt "Oscar-winning Features" (Arbeitstitel)
*Stand: 26.11.2018 [erstellt von Sviat, ergänzt von Anna]*

## *Inhaltliches*

### Untersuchungsgegenstand und Ziel des Projektes
[aktualisiert Dez. 2018: ]
* Das Projekt "Oscar-Winning Features" (Arbeitstitel, alternativ: Award-Winning Movie Features) ist der Erforschung von Merkmalen preisgekrönter Filme gewidmet. 
* Ziel des Projektes ist es, möglichst allgemeine Tendenzen für "Erfolgsmerkmale" prämierter Filme sowie versteckte Zusammenhänge zwischen unterschiedlichen Merkmalen (wie z. B. Titel, Erscheinungsjahr, Genre, Handlungsbeschreibung etc.) zu entdecken. 
* Dabei beschränken wir uns auf die Analyse bzw. Auswertung textueller Daten von Filmen, die im Rahmen unterschiedlicher Preisverleihungen und Ranglisten als "Bester Film" eines jeweiligen Jahres bezeichnet wurden (z. B. Oscar, Golden Globe oder Platz 1 der weltweit höchsten Einspielergebnisse eines Jahres). 
* Die Daten und Untersuchungsergebnisse werden auf einer Website visualisiert bzw. präsentiert.

[alt:]
* Inhalt des Projektes: Eine Analyse bzw. Auswertung textueller Daten von preisgekrönten Filmen. (Welche, wie viele?)
* Dabei sollen u. a. folgende Features berücksichtigt werden: Titel, Erscheinungsjahr, Genre, Handlungsbeschreibung etc. (Was noch?)
* Ziel des Projektes ist es, möglichst allgemeine Tendenzen für die "erfolgreichen" Filme sowie versteckte Zusammenhänge zwischen unterschiedlichen Merkmalen zu entdecken.
* [Vorgehensweise: explorativ]

### Methoden
 
* Textanalyse 
  * Topic Modelling
  * statistische Analyse
  * Machine Learning?
  * ...
* Informationsvisualisierung

### Teilaufgaben
* Initialisierung
  * Allgemeine Recherchen
  * Definition von Zielen und Ressourcen
* Planung (fortlaufend)
  * Allgemeine Recherchen
  * Anfertigung eines Projektplanes mit Milestones und Aufgabenverteilung
* Ausführung <-> Qualitätssicherung
  * Recherchen zu Daten
  * Datenerhebung
  * Datenaufbereitung
  * Recherchen zu Analysetools von Daten
  * Implementierung der Analysetools
  * Visualisierung von Daten
  * Repräsentation der Auswertungen in Form einer Webanwendung 
  * Evaluation der Ergebnisse
* Abschluss
  * Präsentation der Ergebnisse vor KommilitonInnen
  * Launch der Webanwendung

## *Organisatorisches*

### Zeitliche und personelle Einschränkungen
* 3 Personen
* Abgabefrist: 15.03.2019

### Komplexität und Risiken
Das Projekt kann als "kompliziert" bis "komplex" bezeichnet werden. Da es sich um eine explorative Untersuchung handelt, besteht das Risiko, dass die Ergebnisse eine begrenzte Aussagekraft besitzen. 

### Projektmanagement-Strategie
Da das Projekt einerseits ein klar definiertes Ziel hat, aber anderseits eine explorative Untersuchung mit begrenzten zeitlichen und personellen Ressourcen darstellt, werden sowohl klassische als auch agile Projektmanagement-Strategien verwendet. 
