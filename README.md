# Oscar-Winning Features

## Projektpräsentation in ILIAS: 

*Feb. 2019*:
“Visualising Film Features” is a web application whose aim is to collect and process data about award-winning and/or highly-ranked films from free online film data sources and explore visualisation techniques in order to present and (if possible) analyse or interpret the film data. The project results should serve as a model for processing and visualising larger film datasets in the future. Visualisations and findings shall be presented in a web application. Furthermore, project results are presented in a detailed project documentation.

*Nov. 2018*:
Das Projekt "Oscar-Winning Features" (Arbeitstitel) ist der Erforschung von Merkmalen preisgekrönter Filme gewidmet. Ziel des Projektes ist es, möglichst allgemeine Tendenzen für "Erfolgsmerkmale" prämierter Filme sowie versteckte Zusammenhänge zwischen unterschiedlichen Merkmalen (wie z. B. Titel, Erscheinungsjahr, Genre, Handlungsbeschreibung etc.) zu entdecken. Dabei beschränken wir uns auf die Analyse bzw. Auswertung textueller Daten von Oscar-prämierten Filmen. Die Ergebnisse werden auf einer Website visualisiert/präsentiert.

*Dec. 2018*
Das Projekt "Oscar-Winning Features" (Arbeitstitel, alternativ: Award-Winning Movie Features) ist der Erforschung von Merkmalen preisgekrönter Filme gewidmet. Ziel des Projektes ist es, möglichst allgemeine Tendenzen für "Erfolgsmerkmale" prämierter Filme sowie versteckte Zusammenhänge zwischen unterschiedlichen Merkmalen (wie z. B. Titel, Erscheinungsjahr, Genre, Handlungsbeschreibung etc.) zu entdecken. Dabei beschränken wir uns auf die Analyse bzw. Auswertung textueller Daten von Filmen, die im Rahmen unterschiedlicher Preisverleihungen und Ranglisten als "Bester Film" eines jeweiligen Jahres bezeichnet wurden (z. B. Oscar, Golden Globe oder Platz 1 der weltweit höchsten Einspielergebnisse eines Jahres). Die Daten und Untersuchungsergebnisse werden auf einer Website visualisiert bzw. präsentiert.