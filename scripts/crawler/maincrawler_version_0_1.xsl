<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    exclude-result-prefixes="xs"
    version="2.0">
    
    <xsl:strip-space elements="*"/>
    <xsl:output method="xml" indent="yes"></xsl:output>
    
    <xsl:template match="/">
        <!-- 1. 
            - Add film article links from EN Wikipedia to the variable linksToEnglishWikipedia between <linksList> </linksList>, e.g.:
            
            <film><link>https://en.wikipedia.org/wiki/Intolerance_(film)</link><prizeYear>1916</prizeYear></film>
			-  set the prize name to variable "prizeName"      
        -->
        <xsl:variable name="prizeName" select="'golden-palm'"/>
        <xsl:variable name="linksToEnglishWikipedia">
            <linksList>
             </linksList>
        </xsl:variable>
                <!-- @project_id alphabetisch sortierte Liste 1, 2, 3 ... -->
        
        <!-- 3. Get the data from the english Wikipedia--> 
        <data>
            <xsl:for-each select="$linksToEnglishWikipedia//film">
                <xsl:variable name="wLink" select="link"/>
                <xsl:variable name="prizeYear" select="prizeYear"/>
                <xsl:variable name="fullData" select="doc($wLink)//body"/>
                <film id="{39+position()}" prize="{$prizeName}">
                <link><xsl:value-of select="$wLink"/></link>
                <prizeYear><xsl:value-of select="$prizeYear"/></prizeYear>
                <originId>
                    <xsl:value-of select="$fullData//li/a[contains(@href,'www.imdb.com/title')]/substring-before(substring-after(@href,'title/'),'/')"></xsl:value-of> 
                </originId>
                <img>
                    <xsl:attribute name="src"><xsl:value-of select="concat('https:',$fullData//table[@class='infobox vevent']//a[@class='image']/img/@src)"/></xsl:attribute>
                </img>
                <xsl:variable name="getInfoBox" select="$fullData//table[@class='infobox vevent']"/>
                <originalTitle><xsl:value-of select="$fullData//$getInfoBox/tbody/tr[1]/th[1]"></xsl:value-of></originalTitle>
                <!-- Get director list -->
                <directorList>
                    <xsl:choose>
                        <xsl:when test="$getInfoBox//tr[th='Directed by']/td/exists(div[@class='plainlist'])">
                            <xsl:for-each select="$getInfoBox//tr[th='Directed by']/td/div[@class='plainlist']/ul/li">
                                <director><xsl:value-of select="."/></director>
                            </xsl:for-each>
                        </xsl:when>
                        <xsl:otherwise>
                            <xsl:if test="not($getInfoBox//tr[th='Directed by']/td/br)">
                                <director><xsl:value-of select="$getInfoBox//tr[th='Directed by']/td"/></director>
                            </xsl:if>
                                <xsl:variable name="mergeListDirector">
                                    <item><xsl:value-of select="$getInfoBox//tr[th='Directed by']/td/br[1]/preceding-sibling::*"/></item>
                                    <xsl:for-each select="$getInfoBox//tr[th='Directed by']/td/br/following-sibling::*">
                                        <item><xsl:value-of select="."/></item>
                                    </xsl:for-each>
                                    <item><xsl:value-of select="$getInfoBox//tr[th='Directed by']/td/br[1]/preceding-sibling::text()"/></item>
                                    <xsl:for-each select="$getInfoBox//tr[th='Directed by']/td/br/following-sibling::text()">
                                        <item><xsl:value-of select="."/></item>
                                    </xsl:for-each>
                                </xsl:variable>
                                <xsl:for-each select="$mergeListDirector/item[.!=''][not(contains(.,'('))]">
                                    <director><xsl:value-of select="normalize-space(.)"/></director>
                                </xsl:for-each>
                        </xsl:otherwise>
                    </xsl:choose>
                </directorList>
                
                <!-- Get producer list -->
                <producerList>
                    <xsl:choose>
                        <xsl:when test="$getInfoBox//tr[th='Produced by']/td/exists(div[@class='plainlist'])">
                            <xsl:for-each select="$getInfoBox//tr[th='Produced by']/td/div[@class='plainlist']/ul/li">
                                <producer><xsl:value-of select="."/></producer>
                            </xsl:for-each>
                        </xsl:when>
                        <xsl:otherwise>
                            <xsl:if test="not($getInfoBox//tr[th='Produced by']/td/br)">
                                <producer><xsl:value-of select="$getInfoBox//tr[th='Produced by']/td"/></producer>
                            </xsl:if>
                                <xsl:variable name="mergeListProducer">
                                    <item><xsl:value-of select="$getInfoBox//tr[th='Produced by']/td/br[1]/preceding-sibling::*"/></item>
                                    <xsl:for-each select="$getInfoBox//tr[th='Produced by']/td/br/following-sibling::*">
                                        <item><xsl:value-of select="."/></item>
                                    </xsl:for-each>
                                    <item><xsl:value-of select="$getInfoBox//tr[th='Produced by']/td/br[1]/preceding-sibling::text()"/></item>
                                    <xsl:for-each select="$getInfoBox//tr[th='Produced by']/td/br/following-sibling::text()">
                                        <item><xsl:value-of select="."/></item>
                                    </xsl:for-each>
                                </xsl:variable>
                            <xsl:for-each select="$mergeListProducer/item[.!=''][not(contains(.,'('))]">
                                    <producer><xsl:value-of select="normalize-space(.)"/></producer>
                                </xsl:for-each>
                        </xsl:otherwise>
                    </xsl:choose>
                </producerList>
                <!-- Get composer list -->
                <composerList>
                    <xsl:choose>
                        <xsl:when test="$getInfoBox//tr[th='Music by']/td/exists(div[@class='plainlist'])">
                            <xsl:for-each select="$getInfoBox//tr[th='Music by']/td/div[@class='plainlist']/ul/li">
                                <composer><xsl:value-of select="normalize-space(.)"/></composer>
                            </xsl:for-each>
                        </xsl:when>
                        <xsl:otherwise>
                            <xsl:if test="not($getInfoBox//tr[th='Music by']/td/br)">
                                <composer><xsl:value-of select="$getInfoBox//tr[th='Music by']/td"/></composer>
                            </xsl:if>
                            <xsl:variable name="mergeListMusic">
                                <item><xsl:value-of select="$getInfoBox//tr[th='Music by']/td/br[1]/preceding-sibling::*"/></item>
                                <xsl:for-each select="$getInfoBox//tr[th='Music by']/td/br/following-sibling::*">
                                    <item><xsl:value-of select="."/></item>
                                </xsl:for-each>
                                <item><xsl:value-of select="$getInfoBox//tr[th='Music by']/td/br[1]/preceding-sibling::text()"/></item>
                                <xsl:for-each select="$getInfoBox//tr[th='Music by']/td/br/following-sibling::text()">
                                    <item><xsl:value-of select="."/></item>
                                </xsl:for-each>
                            </xsl:variable>
                            <xsl:for-each select="$mergeListMusic/item[.!=''][not(contains(.,'('))]">
                                <composer><xsl:value-of select="normalize-space(.)"/></composer>
                            </xsl:for-each>
                        </xsl:otherwise>
                    </xsl:choose>
                </composerList>
                
                <!-- Get writter list -->
                <writterList>
                    <xsl:choose>
                        <xsl:when test="$getInfoBox//tr[th='Written by']/td/exists(div[@class='plainlist'])">
                            <xsl:for-each select="$getInfoBox//tr[th='Written by']/td/div[@class='plainlist']/ul/li">
                                <writter><xsl:value-of select="normalize-space(.)"/></writter>
                            </xsl:for-each>
                        </xsl:when>
                        <xsl:otherwise>
                            <xsl:if test="not($getInfoBox//tr[th='Written by']/td/br)">
                                <writter><xsl:value-of select="$getInfoBox//tr[th='Written by']/td"/></writter>
                            </xsl:if>
                            <xsl:variable name="mergeListWritter">
                                <item><xsl:value-of select="$getInfoBox//tr[th='Written by']/td/br[1]/preceding-sibling::*"/></item>
                                <xsl:for-each select="$getInfoBox//tr[th='Written by']/td/br/following-sibling::*">
                                    <item><xsl:value-of select="."/></item>
                                </xsl:for-each>
                                <item><xsl:value-of select="$getInfoBox//tr[th='Written by']/td/br[1]/preceding-sibling::text()"/></item>
                                <xsl:for-each select="$getInfoBox//tr[th='Written by']/td/br/following-sibling::text()">
                                    <item><xsl:value-of select="."/></item>
                                </xsl:for-each>
                            </xsl:variable>
                            <xsl:for-each select="$mergeListWritter/item[.!=''][not(contains(.,'('))]">
                                <writter><xsl:value-of select="normalize-space(.)"/></writter>
                            </xsl:for-each>
                        </xsl:otherwise>
                    </xsl:choose>
                </writterList>
                
                <!-- Get cinematographer list -->
                <cinematographerList>
                    <xsl:choose>
                        <xsl:when test="$getInfoBox//tr[th='Cinematography']/td/exists(/div[@class='plainlist'])">
                            <xsl:for-each select="$getInfoBox//tr[th='Cinematography']/td/div[@class='plainlist']/ul/li">
                                <cinematographer><xsl:value-of select="normalize-space(.)"/></cinematographer>
                            </xsl:for-each>
                        </xsl:when>
                        <xsl:otherwise>
                            <xsl:if test="not($getInfoBox//tr[th='Cinematography']/td/br)">
                                <cinematographer><xsl:value-of select="$getInfoBox//tr[th='Cinematography']/td"/></cinematographer>
                            </xsl:if>
                            <xsl:variable name="mergeListCinematographer">
                                <item><xsl:value-of select="$getInfoBox//tr[th='Cinematography']/td/br[1]/preceding-sibling::*"/></item>
                                <xsl:for-each select="$getInfoBox//tr[th='Cinematography']/td/br/following-sibling::*">
                                    <item><xsl:value-of select="."/></item>
                                </xsl:for-each>
                                <item><xsl:value-of select="$getInfoBox//tr[th='Cinematography']/td/br[1]/preceding-sibling::text()"/></item>
                                <xsl:for-each select="$getInfoBox//tr[th='Cinematography']/td/br/following-sibling::text()">
                                    <item><xsl:value-of select="."/></item>
                                </xsl:for-each>
                            </xsl:variable>
                            <xsl:for-each select="$mergeListCinematographer/item[.!=''][not(contains(.,'('))]">
                                <cinematograph><xsl:value-of select="normalize-space(.)"/></cinematograph>
                            </xsl:for-each>
                        </xsl:otherwise>
                    </xsl:choose>
                </cinematographerList>
                
                <!-- Get production company list -->
                <productionCompanyList> 
                    <xsl:if test="not($getInfoBox//tr[contains(th,'Production')]/td/br)">
                        <production_company><xsl:value-of select="$getInfoBox//tr[contains(th,'Production')]/td"/></production_company>
                    </xsl:if>
                    <xsl:variable name="mergeListProductionCompany">
                        <item><xsl:value-of select="$getInfoBox//tr[contains(th,'Production')]/td/br[1]/preceding-sibling::*"/></item>
                        <xsl:for-each select="$getInfoBox//tr[contains(th,'Production')]/td/br/following-sibling::*">
                            <item><xsl:value-of select="."/></item>
                        </xsl:for-each>
                        <item><xsl:value-of select="$getInfoBox//tr[contains(th,'Production')]/td/br[1]/preceding-sibling::text()"/></item>
                        <xsl:for-each select="$getInfoBox//tr[contains(th,'Production')]/td/br/following-sibling::text()">
                            <item><xsl:value-of select="."/></item>
                        </xsl:for-each>
                    </xsl:variable>
                    <xsl:for-each select="$mergeListProductionCompany/item[.!=''][not(contains(.,'('))]">
                        <production_company><xsl:value-of select="normalize-space(.)"/></production_company>
                    </xsl:for-each>
                </productionCompanyList>
                
                <budget><xsl:value-of select="$getInfoBox//tr[contains(.,'Budget')]/replace(replace(td,'\.|\$|\[.*\]',''),' million| billion','000000')"/></budget>
                <box><xsl:value-of select="$getInfoBox//tr[contains(.,'Box')]/replace(replace(td,'\.|\$|\[.*\]',''),' million| billion','000000')"/></box>
                <plot>
                    <xsl:value-of select="$fullData//p[preceding::h2/span[@id='Plot'] and not(preceding::h2/span[@id!='Plot'])]"/>
                </plot>
            </film>
        </xsl:for-each>
        </data>
    </xsl:template>
    
</xsl:stylesheet>