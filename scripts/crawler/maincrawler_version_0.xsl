<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    exclude-result-prefixes="xs"
    version="2.0">
    
    <xsl:strip-space elements="*"/>
    <xsl:output method="xml" indent="yes"></xsl:output>
    
    <xsl:template match="/">
        <!-- 1. 
            - Add film article links from GERMAN(!) Wikipedia to the variable linksToGermanWikipedia between <linksList> </linksList>, e.g.:
            
            <film><link>https://de.wikipedia.org/wiki/Fl%C3%BCgel_aus_Stahl</link><prizeYear>1929</prizeYear></film>
			<film><link>https://de.wikipedia.org/wiki/The_Broadway_Melody</link><prizeYear>1930</prizeYear></film>
			         
			-  set the prize name to variable "prizeName"
			         
        -->
        <xsl:variable name="prizeName" select="'oscar_best_film'"/>
        <xsl:variable name="linksToGermanWikipedia">
            <linksList>
             </linksList>
        </xsl:variable>
        <!-- 2. Get the data from the german Wikipedia-->
        <xsl:variable name="getFromGermanWikipedia">
            <!-- TO-DO: Übersetzung bzw. Klassifizierung (zunächst nicht alle Genres, sondern nur welche in Daten vorkommen. In "Oscar Bester Film kommen" folgende vor:
                Erster Weltkrieg im Film
Kriegsfilm
Filmdrama
Liebesfilm
Anti-Kriegsfilm
Western
Roadmovie
Screwball-Komödie
Abenteuerfilm
Filmkomödie
Sezessionskrieg im Film
Historienfilm
Kriminalfilm
Thriller
Zweiter Weltkrieg im Film
Zirkusfilm
Mafiafilm
Römische Geschichte im Film
Filmbiografie
Actionfilm
Neo-Noir
Heist-Movie
Psychodrama
Anti-Western
Horrorfilm
Katastrophenfilm
Satirefilm
Gefängnisfilm
Fantasyfilm
Agentenfilm
Schwarze Komödie


            -->
        <xsl:variable name="lGenre">
            <genre>Abenteuerfilm‎</genre>
            <genre>Mantel-und-Degen-Film‎</genre>
            <genre>Piratenfilm‎</genre>
            <genre>Ritterfilm‎</genre>
            <genre>Kreuzzüge im Film‎</genre>
            <genre>Film über Ivanhoe‎</genre>
            <genre>Actionfilm‎</genre>
            <genre>Agentenfilm‎</genre>
            <genre>James-Bond-Film‎</genre>
            <genre>Bergfilm‎</genre>
            <genre>Bergsportfilm‎</genre>
            <genre>Filmbiografie‎</genre>
            <genre>Buddy-Film‎</genre>
            <genre g="drama">Filmdrama‎</genre>
            <genre g="drama">Doku-Drama‎</genre>
            <genre g="drama">Psychodrama‎</genre>
            <genre>Erotikfilm‎</genre>
            <genre>Pinku eiga‎</genre>
            <genre>Pornofilm‎</genre>
            <genre>Pornofilmreihe‎</genre>
            <genre>Fantasyfilm‎</genre>
            <genre>Märchenfilm‎</genre>
            <genre>Found-Footage-Film‎</genre>
            <genre>Gerichtsfilm‎</genre>
            <genre>Heimatfilm‎</genre>
            <genre>Historienfilm‎</genre>
            <genre>Bananenkriege im Film‎</genre>
            <genre>Deutsch-Französischer Krieg im Film‎</genre>
            <genre>Erster Weltkrieg im Film‎</genre>
            <genre>Propagandafilm</genre>
            <genre>Koalitionskriege im Film‎</genre>
            <genre>Kreuzzüge im Film‎</genre>
            <genre>Film über Ivanhoe‎</genre>
            <genre>Krieg in der Antike im Film‎</genre>
            <genre>Römische Geschichte im Film‎</genre>
            <genre>Asterixverfilmung‎</genre>
            <genre>Sezessionskrieg im Film‎</genre>
            <genre>Zweiter Weltkrieg im Film‎</genre>
            <genre>Propagandafilm</genre>
            <genre>Horrorfilm‎</genre>
            <genre>Backwoods-Film‎</genre>
            <genre>Dr.-Jekyll-und-Mr.-Hyde-Film‎</genre>
            <genre>Frankenstein-Film‎</genre>
            <genre>Geisterfilm‎</genre>
            <genre>Kannibalenfilm‎</genre>
            <genre>Mumienfilm‎</genre>
            <genre>Slasher-Film‎</genre>
            <genre>Tierhorrorfilm‎</genre>
            <genre>Vampirfilm‎</genre>
            <genre>Dracula-Film‎</genre>
            <genre>Werwolffilm‎</genre>
            <genre>Zombiefilm‎</genre>
            <genre>Katastrophenfilm‎</genre>
            <genre>Filmkomödie‎</genre>
            <genre>Filmparodie‎</genre>
            <genre>Commedia all’italiana‎</genre>
            <genre>Culture-Clash-Komödie‎</genre>
            <genre>Satirefilm‎</genre>
            <genre>Schwarze Komödie‎</genre>
            <genre>Screwball-Komödie‎</genre>
            <genre>Slapstick-Film‎</genre>
            <genre>Stoner-Movie‎</genre>
            <genre>Kriegsfilm‎</genre>
            <genre>Anti-Kriegsfilm‎</genre>
            <genre>Kriminalfilm‎</genre>
            <genre>Der Bulle von Tölz</genre>
            <genre>Das Duo‎</genre>
            <genre>Film noir‎</genre>
            <genre>Neo-Noir‎</genre>
            <genre>Gangsterfilm‎</genre>
            <genre>Gefängnisfilm‎</genre>
            <genre>Heist-Movie‎</genre>
            <genre>Mafiafilm‎</genre>
            <genre>Yakuza-Film‎</genre>
            <genre>Poliziottesco‎</genre>
            <genre>Helen Dorn‎</genre>
            <genre>Kommissarin Lucas‎</genre>
            <genre>Marie Brand‎</genre>
            <genre>Spreewaldkrimi‎</genre>
            <genre>Der Staatsanwalt hat das Wort‎</genre>
            <genre>Ein starkes Team‎</genre>
            <genre>Unter anderen Umständen</genre>
            <genre>Liebesfilm‎</genre>
            <genre>Martial-Arts-Film‎</genre>
            <genre>Mondo‎</genre>
            <genre>Roadmovie‎</genre>
            <genre>Sandalenfilm‎</genre>
            <genre>Science-Fiction-Film‎</genre>
            <genre>Dr.-Jekyll-und-Mr.-Hyde-Film‎</genre>
            <genre>Endzeitfilm‎</genre>
            <genre>Frankenstein-Film‎</genre>
            <genre>Star-Trek-Kinofilm‎</genre>
            <genre>Star-Wars-Film‎</genre>
            <genre>Splatterfilm‎</genre>
            <genre>Thriller‎</genre>
            <genre>Giallo‎</genre>
            <genre>Politthriller‎</genre>
            <genre>Weihnachtsfilm‎</genre>
            <genre>Western‎</genre>
            <genre>Anti-Western‎</genre>
            <genre>DEFA-Indianerfilm‎</genre>
            <genre>Eurowestern‎</genre>
            <genre>Italowestern‎</genre>
            <genre>Revolutionswestern‎</genre>
            <genre>Zirkusfilm‎</genre>
        </xsl:variable>
            <!-- TO-DO: Übersetzung bzw. Klassifizierung (zunächst nicht alle Genres, sondern nur welche in Daten vorkommen. In "Oscar Bester Film kommen" folgende vor: 
            Schwarzweißfilm
Stummfilm
Musicalfilm
Literaturverfilmung
Farbfilm
Monumentalfilm
Tanzfilm
Filmbiografie
Independentfilm
Boxerfilm
Filmbiografie über Sportler
Laufsportfilm
Olympiafilm
3D-Film
Episodenfilm
Jugendfilm
            -->
        <xsl:variable name="lGattung">
            <gattung>3D-Film‎</gattung>
            <gattung>4D-Film‎</gattung>
            <gattung>Direct-to-Video-Produktion‎</gattung>
            <gattung>Original Video Animation‎</gattung>
            <gattung>Dokumentarfilm‎</gattung>
            <gattung>Doku-Drama‎</gattung>
            <gattung>Langzeitdokumentarfilm‎</gattung>
            <gattung>Episodenfilm‎</gattung>
            <gattung>Experimentalfilm‎</gattung>
            <gattung>Exploitationfilm‎</gattung>
            <gattung>Blaxploitation‎</gattung>
            <gattung>Naziploitation‎</gattung>
            <gattung>Familienfilm‎</gattung>
            <gattung>Farbfilm‎</gattung>
            <gattung>Fernsehfilm‎</gattung>
            <gattung>Das Duo‎</gattung>
            <gattung>Helen Dorn‎</gattung>
            <gattung>Kommissarin Lucas‎</gattung>
            <gattung>Marie Brand‎</gattung>
            <gattung>Nachtschicht</gattung>
            <gattung>Polizeiruf 110</gattung>
            <gattung>Spreewaldkrimi‎</gattung>
            <gattung>Der Staatsanwalt hat das Wort‎</gattung>
            <gattung>Ein starkes Team‎</gattung>
            <gattung>Tatort</gattung>
            <gattung>Unter anderen Umständen</gattung>
            <gattung>Independentfilm‎</gattung>
            <gattung>Jugendfilm‎</gattung>
            <gattung>Kinderfilm‎</gattung>
            <gattung>Kurzfilm‎</gattung>
            <gattung>Musikvideo‎</gattung>
            <gattung>Literaturverfilmung‎</gattung>
            <gattung>Literaturverfilmung nach Autor‎</gattung>
            <gattung>Bibelverfilmung‎</gattung>
            <gattung>Bibelverfilmung</gattung>
            <gattung>Bibelverfilmung</gattung>
            <gattung>Comicverfilmung‎</gattung>
            <gattung>Asterixverfilmung‎</gattung>
            <gattung>Film über Batman‎</gattung>
            <gattung>Film über Captain America‎</gattung>
            <gattung>Film über Spider-Man‎</gattung>
            <gattung>Film über Superman‎</gattung>
            <gattung>Frankenstein-Film‎</gattung>
            <gattung>Märchenfilm‎</gattung>
            <gattung>Low-Budget-Film‎</gattung>
            <gattung>Milieustudie</gattung>
            <gattung>Mockbuster‎</gattung>
            <gattung>Mockumentary</gattung>
            <gattung>Monumentalfilm‎</gattung>
            <gattung>Musikfilm‎</gattung>
            <gattung>Musicalfilm‎</gattung>
            <gattung>Jazzfilm‎</gattung>
            <gattung>Filmbiografie über Jazzmusiker‎</gattung>
            <gattung>Tanzfilm‎</gattung>
            <gattung>Musikvideo‎</gattung>
            <gattung>Naturfilm‎</gattung>
            <gattung>No-Budget-Film‎</gattung>
            <gattung>Schwarzweißfilm‎</gattung>
            <gattung>Serial‎</gattung>
            <gattung>Sportfilm‎</gattung>
            <gattung>Baseballfilm‎</gattung>
            <gattung>Basketballfilm‎</gattung>
            <gattung>Bergsportfilm‎</gattung>
            <gattung>Bowlingfilm‎</gattung>
            <gattung>Boxerfilm‎</gattung>
            <gattung>Cheerleadingfilm‎</gattung>
            <gattung>Eishockeyfilm‎</gattung>
            <gattung>Eiskunstlauffilm‎</gattung>
            <gattung>Filmbiografie über Sportler‎</gattung>
            <gattung>Footballfilm‎</gattung>
            <gattung>Fußballfilm‎</gattung>
            <gattung>Golffilm‎</gattung>
            <gattung>Handballfilm‎</gattung>
            <gattung>Laufsportfilm‎</gattung>
            <gattung>Martial-Arts-Film‎</gattung>
            <gattung>Motorsportfilm‎</gattung>
            <gattung>Olympiafilm‎</gattung>
            <gattung>Parkourfilm‎</gattung>
            <gattung>Pferdesportfilm‎</gattung>
            <gattung>Radsportfilm‎</gattung>
            <gattung>Ringerfilm‎</gattung>
            <gattung>Ruderfilm‎</gattung>
            <gattung>Rugbyfilm‎</gattung>
            <gattung>Segelsportfilm‎</gattung>
            <gattung>Skisportfilm‎</gattung>
            <gattung>Surffilm‎</gattung>
            <gattung>Turnerfilm‎</gattung>
            <gattung>Wrestlingfilm‎</gattung>
            <gattung>Stummfilm‎</gattung>
            <gattung>Tanzfilm‎</gattung>
            <gattung>Tierfilm‎</gattung>
            <gattung>Hundefilm‎</gattung>
            <gattung>Pferdefilm‎</gattung>
            <gattung>My Little Pony</gattung>
            <gattung>Pferdesportfilm‎</gattung>
            <gattung>Tierhorrorfilm‎</gattung>
            <gattung>Trickfilm‎</gattung>
            <gattung>Anime-Film‎</gattung>
            <gattung>Computeranimationsfilm‎</gattung>
            <gattung>Motion-Capture-Film‎</gattung>
            <gattung>Öl-auf-Glas-Animationsfilm‎</gattung>
            <gattung>Stop-Motion-Film‎</gattung>
            <gattung>Knetanimationsfilm‎</gattung>
            <gattung>Zeichentrickfilm‎</gattung>
            <gattung>Unterrichtsfilm‎</gattung>
            <gattung>Versionenfilm‎</gattung>
            <gattung>Videospielverfilmung‎</gattung>
            <gattung>Werbefilm‎</gattung>
        </xsl:variable>
        <xsl:variable name="lCountry" as="node()"><list>
            <country>Afghanischer Film‎</country>
            <country>Ägyptischer Film‎</country>
            <country>Algerischer Film‎</country>
            <country>Angolanischer Film‎</country>
            <country>Argentinischer Film‎</country>
            <country>Armenischer Film‎</country>
            <country>Aserbaidschanischer Film‎</country>
            <country>Äthiopischer Film‎</country>
            <country>Australischer Film‎</country>
            <country>Bahamaischer Film‎</country>
            <country>Belgischer Film‎</country>
            <country>Bolivianischer Film‎</country>
            <country>Bosnisch-herzegowinischer Film‎</country>
            <country>Brasilianischer Film‎</country>
            <country en="United Kingdom">Britischer Film‎</country>
            <country>Bulgarischer Film‎</country>
            <country>Burkinischer Film‎</country>
            <country>Chilenischer Film‎</country>
            <country>Chinesischer Film‎</country>
            <country>Dänischer Film‎</country>
            <country>DDR-Film‎</country>
            <country en="Germany">Deutscher Film‎</country>
            <country>Dominikanischer Film‎</country>
            <country>Ecuadorianischer Film‎</country>
            <country>Estnischer Film‎</country>
            <country>Finnischer Film‎</country>
            <country id="France">Französischer Film‎</country>
            <country>Griechischer Film‎</country>
            <country>Haitianischer Film‎</country>
            <country>Hongkong-Film‎</country>
            <country en="India">Indischer Film‎</country>
            <country>Indonesischer Film‎</country>
            <country>Irakischer Film‎</country>
            <country>Iranischer Film‎</country>
            <country>Irischer Film‎</country>
            <country>Isländischer Film‎</country>
            <country>Israelischer Film‎</country>
            <country en="Italy">Italienischer Film‎</country>
            <country>Jamaikanischer Film‎</country>
            <country>Japanischer Film‎</country>
            <country>Jemenitischer Film‎</country>
            <country>Jordanischer Film‎</country>
            <country>Jugoslawischer Film‎</country>
            <country>Kambodschanischer Film‎</country>
            <country en="Canada">Kanadischer Film‎</country>
            <country>Kapverdischer Film‎</country>
            <country>Kasachischer Film‎</country>
            <country>Katarischer Film‎</country>
            <country>Kenianischer Film‎</country>
            <country>Kolumbianischer Film‎</country>
            <country>Kroatischer Film‎</country>
            <country>Kubanischer Film‎</country>
            <country>Laotischer Film‎</country>
            <country>Lettischer Film‎</country>
            <country>Libanesischer Film‎</country>
            <country>Liechtensteinischer Film‎</country>
            <country>Litauischer Film‎</country>
            <country>Luxemburgischer Film‎</country>
            <country>Malaysischer Film‎</country>
            <country>Malischer Film‎</country>
            <country>Maltesischer Film‎</country>
            <country>Marokkanischer Film‎</country>
            <country>Mauretanischer Film‎</country>
            <country>Mazedonischer Film‎</country>
            <country>Mexikanischer Film‎</country>
            <country>Moldauischer Film‎</country>
            <country>Mongolischer Film‎</country>
            <country>Mosambikanischer Film‎</country>
            <country>Namibischer Film‎</country>
            <country en="New Zealand">Neuseeländischer Film‎</country>
            <country>Nicaraguanischer Film‎</country>
            <country>Niederländischer Film‎</country>
            <country>Nigerianischer Film‎</country>
            <country>Nordkoreanischer Film‎</country>
            <country>Norwegischer Film‎</country>
            <country>Österreichischer Film‎</country>
            <country>Osttimoresischer Film‎</country>
            <country>Pakistanischer Film‎</country>
            <country>Panamaischer Film‎</country>
            <country>Paraguayischer Film‎</country>
            <country>Peruanischer Film‎</country>
            <country>Philippinischer Film‎</country>
            <country>Polnischer Film‎</country>
            <country>Portugiesischer Film‎</country>
            <country>Rumänischer Film‎</country>
            <country>Russischer Film‎</country>
            <country>Saudi-arabischer Film‎</country>
            <country>Schwedischer Film‎</country>
            <country>Schweizer Film‎</country>
            <country>Senegalesischer Film‎</country>
            <country>Serbischer Film‎</country>
            <country>Singapurischer Film‎</country>
            <country>Slowakischer Film‎</country>
            <country>Slowenischer Film‎</country>
            <country>Sowjetischer Film‎</country>
            <country>Spanischer Film‎</country>
            <country>Sri-lankischer Film‎</country>
            <country>Südafrikanischer Film‎</country>
            <country>Südkoreanischer Film‎</country>
            <country>Syrischer Film‎</country>
            <country>Taiwanischer Film‎</country>
            <country>Thailändischer Film‎</country>
            <country>Tschechischer Film‎</country>
            <country>Tschechoslowakischer Film‎</country>
            <country>Tunesischer Film‎</country>
            <country>Türkischer Film‎</country>
            <country>Ugandischer Film‎</country>
            <country>Ukrainischer Film‎</country>
            <country>Ungarischer Film‎</country>
            <country>Uruguayischer Film‎</country>
            <country en="USA">US-amerikanischer Film‎</country>
            <country>Usbekischer Film‎</country>
            <country>Venezolanischer Film‎</country>
            <country>Film der Vereinigten Arabischen Emirate‎</country>
            <country>Vietnamesischer Film‎</country>
            <country>Weißrussischer Film‎</country>
            <country>Zyprischer Film‎</country>
        </list>
        </xsl:variable>
        <xsl:variable name="lYear">
            <year>Filmtitel 1900‎</year>
            <year>Filmtitel 1901‎</year>
            <year>Filmtitel 1902‎</year>
            <year>Filmtitel 1903‎</year>
            <year>Filmtitel 1904‎</year>
            <year>Filmtitel 1905‎</year>
            <year>Filmtitel 1906‎</year>
            <year>Filmtitel 1907‎</year>
            <year>Filmtitel 1908‎</year>
            <year>Filmtitel 1909‎</year>
            <year>Filmtitel 1910‎</year>
            <year>Filmtitel 1911‎</year>
            <year>Filmtitel 1912‎</year>
            <year>Filmtitel 1913‎</year>
            <year>Filmtitel 1914‎</year>
            <year>Filmtitel 1915‎</year>
            <year>Filmtitel 1916‎</year>
            <year>Filmtitel 1917‎</year>
            <year>Filmtitel 1918‎</year>
            <year>Filmtitel 1919‎</year>
            <year>Filmtitel 1920‎</year>
            <year>Filmtitel 1921‎</year>
            <year>Filmtitel 1922‎</year>
            <year>Filmtitel 1923‎</year>
            <year>Filmtitel 1924‎</year>
            <year>Filmtitel 1925‎</year>
            <year>Filmtitel 1926‎</year>
            <year>Filmtitel 1927‎</year>
            <year>Filmtitel 1928‎</year>
            <year>Filmtitel 1929‎</year>
            <year>Filmtitel 1930‎</year>
            <year>Filmtitel 1931‎</year>
            <year>Filmtitel 1932‎</year>
            <year>Filmtitel 1933‎</year>
            <year>Filmtitel 1934‎</year>
            <year>Filmtitel 1935‎</year>
            <year>Filmtitel 1936‎</year>
            <year>Filmtitel 1937‎</year>
            <year>Filmtitel 1938‎</year>
            <year>Filmtitel 1939‎</year>
            <year>Filmtitel 1940‎</year>
            <year>Filmtitel 1941‎</year>
            <year>Filmtitel 1942‎</year>
            <year>Filmtitel 1943‎</year>
            <year>Filmtitel 1944‎</year>
            <year>Filmtitel 1945‎</year>
            <year>Filmtitel 1946‎</year>
            <year>Filmtitel 1947‎</year>
            <year>Filmtitel 1948‎</year>
            <year>Filmtitel 1949‎</year>
            <year>Filmtitel 1950‎</year>
            <year>Filmtitel 1951‎</year>
            <year>Filmtitel 1952‎</year>
            <year>Filmtitel 1953‎</year>
            <year>Filmtitel 1954‎</year>
            <year>Filmtitel 1955‎</year>
            <year>Filmtitel 1956‎</year>
            <year>Filmtitel 1957‎</year>
            <year>Filmtitel 1958‎</year>
            <year>Filmtitel 1959‎</year>
            <year>Filmtitel 1960‎</year>
            <year>Filmtitel 1961‎</year>
            <year>Filmtitel 1962‎</year>
            <year>Filmtitel 1963‎</year>
            <year>Filmtitel 1964‎</year>
            <year>Filmtitel 1965‎</year>
            <year>Filmtitel 1966‎</year>
            <year>Filmtitel 1967‎</year>
            <year>Filmtitel 1968‎</year>
            <year>Filmtitel 1969‎</year>
            <year>Filmtitel 1970‎</year>
            <year>Filmtitel 1971‎</year>
            <year>Filmtitel 1972‎</year>
            <year>Filmtitel 1973‎</year>
            <year>Filmtitel 1974‎</year>
            <year>Filmtitel 1975‎</year>
            <year>Filmtitel 1976‎</year>
            <year>Filmtitel 1977‎</year>
            <year>Filmtitel 1978‎</year>
            <year>Filmtitel 1979‎</year>
            <year>Filmtitel 1980‎</year>
            <year>Filmtitel 1981‎</year>
            <year>Filmtitel 1982‎</year>
            <year>Filmtitel 1983‎</year>
            <year>Filmtitel 1984‎</year>
            <year>Filmtitel 1985‎</year>
            <year>Filmtitel 1986‎</year>
            <year>Filmtitel 1987‎</year>
            <year>Filmtitel 1988‎</year>
            <year>Filmtitel 1989‎</year>
            <year>Filmtitel 1990‎</year>
            <year>Filmtitel 1991‎</year>
            <year>Filmtitel 1992‎</year>
            <year>Filmtitel 1993‎</year>
            <year>Filmtitel 1994‎</year>
            <year>Filmtitel 1995‎</year>
            <year>Filmtitel 1996‎</year>
            <year>Filmtitel 1997‎</year>
            <year>Filmtitel 1998‎</year>
            <year>Filmtitel 1999‎</year>
            <year>Filmtitel 2000‎</year>
            <year>Filmtitel 2001‎</year>
            <year>Filmtitel 2002‎</year>
            <year>Filmtitel 2003‎</year>
            <year>Filmtitel 2004‎</year>
            <year>Filmtitel 2005‎</year>
            <year>Filmtitel 2006‎</year>
            <year>Filmtitel 2007‎</year>
            <year>Filmtitel 2008‎</year>
            <year>Filmtitel 2009‎</year>
            <year>Filmtitel 2010‎</year>
            <year>Filmtitel 2011‎</year>
            <year>Filmtitel 2012‎</year>
            <year>Filmtitel 2013‎</year>
            <year>Filmtitel 2014‎</year>
            <year>Filmtitel 2015‎</year>
            <year>Filmtitel 2016‎</year>
            <year>Filmtitel 2017‎</year>
            <year>Filmtitel 2018‎</year>
            <year>Filmtitel 2019‎</year>
            <year>Filmtitel 2020‎</year>
        </xsl:variable>
        
        <oscarFilms>
            <xsl:for-each select="$linksToGermanWikipedia//film">
                <xsl:variable name="pathToGerman" select="link"></xsl:variable>
                <xsl:variable name="prizeYearV" select="prizeYear"></xsl:variable>
                
            <!-- Help var -->
            <xsl:variable name="categories" as="node()">
                <list>
                <xsl:for-each select="doc($pathToGerman)//div[@id='mw-normal-catlinks']/ul/li/a">
                    <category><xsl:value-of select="."/></category>
                </xsl:for-each>
                </list>
            </xsl:variable>
                <!-- @project_id alphabetisch sortierte Liste 1, 2, 3 ... -->
                <film project_id="">
                    <prizeYear><xsl:value-of select="$prizeYearV"/></prizeYear>
                    <link><xsl:value-of select="doc($pathToGerman)//a[@class='interlanguage-link-target'][.='English']/@href"/></link>
                    <prodYear>
                        <xsl:for-each select="$categories//category">
                            <xsl:variable name="Category1" select="." as="xs:string"/>
                            <xsl:if test="contains(data($lYear),$Category1)">
                                <year><xsl:value-of select="replace(.,'Filmtitel ','')"/></year>
                            </xsl:if>
                            
                        </xsl:for-each>
                    </prodYear>
                    
                    <originId>
                        <xsl:value-of select="doc($pathToGerman)//li/a[contains(@href,'www.imdb.com/title')]/substring-before(substring-after(@href,'title/'),'/')"></xsl:value-of> 
                    </originId>
                    
                    <prodCountries>
                        <xsl:for-each select="$categories//category">
                            <xsl:variable name="Category1" select="." as="xs:string"/>
                                                            <xsl:if test="contains(data($lCountry),$Category1)">
                                                                <country><xsl:value-of select="."/></country>
                                </xsl:if>
                            
                        </xsl:for-each>
                    </prodCountries>

                    <genres>
                        <xsl:for-each select="$categories//category">
                            <xsl:variable name="Category1" select="." as="xs:string"/>
                            <xsl:if test="contains(data($lGenre),$Category1)">
                                <genre><xsl:value-of select="."/></genre>
                            </xsl:if>
                            
                        </xsl:for-each>
                    </genres>
                    
                    <types>
                        <xsl:for-each select="$categories//category">
                            <xsl:variable name="Category1" select="." as="xs:string"/>
                            <xsl:if test="contains(data($lGattung),$Category1)">
                                <type><xsl:value-of select="."/></type>
                            </xsl:if>
                            
                        </xsl:for-each>
                    </types>
                </film>
            
        </xsl:for-each>
        </oscarFilms>
        </xsl:variable>
        
        <!-- 3. Get the data from the english Wikipedia--> 
        <data>
        <xsl:for-each select="$getFromGermanWikipedia//film">
            <film id="{position()}" prize="{$prizeName}">
                <xsl:variable name="path"><xsl:value-of select="link"/></xsl:variable>   
                <xsl:copy-of select="link"></xsl:copy-of>
                <img>
                    <xsl:attribute name="src"><xsl:value-of select="concat('https:',doc($path)//table[@class='infobox vevent']//a[@class='image']/img/@src)"/></xsl:attribute>
                </img>
                <xsl:variable name="getInfoBox" select="doc($path)//table[@class='infobox vevent']"/>
                <originalTitle><xsl:value-of select="doc($path)//$getInfoBox/tbody/tr[1]/th[1]"></xsl:value-of></originalTitle>
                <xsl:copy-of select="originId"></xsl:copy-of>
                <xsl:copy-of select="prodYear/year"></xsl:copy-of>
                <xsl:copy-of select="prizeYear"/>
                <xsl:copy-of select="prodCountries"></xsl:copy-of>
                
                
                <!-- Get director list -->
                <directorList>
                    <xsl:choose>
                        <xsl:when test="$getInfoBox//tr[th='Directed by']/td/exists(div[@class='plainlist'])">
                            <xsl:for-each select="$getInfoBox//tr[th='Directed by']/td/div[@class='plainlist']/ul/li">
                                <director><xsl:value-of select="."/></director>
                            </xsl:for-each>
                        </xsl:when>
                        <xsl:otherwise>
                            <director><xsl:value-of select="$getInfoBox//tr[th='Directed by']/td"/></director>
                        </xsl:otherwise>
                    </xsl:choose>
                </directorList>
                
                <!-- Get producer list -->
                <producerList>
                    <xsl:choose>
                        <xsl:when test="$getInfoBox//tr[th='Produced by']/td/exists(div[@class='plainlist'])">
                            <xsl:for-each select="$getInfoBox//tr[th='Produced by']/td/div[@class='plainlist']/ul/li">
                                <producer><xsl:value-of select="."/></producer>
                            </xsl:for-each>
                        </xsl:when>
                        <xsl:otherwise>
                            <producer><xsl:value-of select="$getInfoBox//tr[th='Produced by']/td"/></producer>
                        </xsl:otherwise>
                    </xsl:choose>
                </producerList>
                
                <!-- Get starring list -->
                <starringList>
                    <xsl:choose>
                        <xsl:when test="$getInfoBox//tr[th='Starring']/td/exists(div[@class='plainlist'])">
                            <xsl:for-each select="$getInfoBox//tr[th='Starring']/td/div[@class='plainlist']/ul/li">
                                <actor><xsl:value-of select="normalize-space(.)"/></actor>
                            </xsl:for-each>
                        </xsl:when>
                        <xsl:otherwise>
                            <actor><xsl:value-of select="$getInfoBox//tr[th='Starring']/td"/></actor>
                        </xsl:otherwise>
                    </xsl:choose>
                </starringList>
                
                <!-- Get composer list -->
                <composerList>
                    <xsl:choose>
                        <xsl:when test="$getInfoBox//tr[th='Music by']/td/exists(div[@class='plainlist'])">
                            <xsl:for-each select="$getInfoBox//tr[th='Music by']/td/div[@class='plainlist']/ul/li">
                                <composer><xsl:value-of select="normalize-space(.)"/></composer>
                            </xsl:for-each>
                        </xsl:when>
                        <xsl:otherwise>
                            <composer><xsl:value-of select="$getInfoBox//tr[th='Music by']/td"/></composer>
                        </xsl:otherwise>
                    </xsl:choose>
                </composerList>
                
                <!-- Get writter list -->
                <writterList>
                    <xsl:choose>
                        <xsl:when test="$getInfoBox//tr[th='Written by']/td/exists(div[@class='plainlist'])">
                            <xsl:for-each select="$getInfoBox//tr[th='Written by']/td/div[@class='plainlist']/ul/li">
                                <writter><xsl:value-of select="normalize-space(.)"/></writter>
                            </xsl:for-each>
                        </xsl:when>
                        <xsl:otherwise>
                            <writter><xsl:value-of select="$getInfoBox//tr[th='Written by']/td"/></writter>
                        </xsl:otherwise>
                    </xsl:choose>
                </writterList>
                
                <!-- Get cinematographer list -->
                <cinematographerList>
                    <xsl:choose>
                        <xsl:when test="$getInfoBox//tr[th='Cinematography']/td/exists(/div[@class='plainlist'])">
                            <xsl:for-each select="$getInfoBox//tr[th='Cinematography']/td/div[@class='plainlist']/ul/li">
                                <cinematographer><xsl:value-of select="normalize-space(.)"/></cinematographer>
                            </xsl:for-each>
                        </xsl:when>
                        <xsl:otherwise>
                            <cinematographer><xsl:value-of select="$getInfoBox//tr[th='Cinematography']/td"/></cinematographer>
                        </xsl:otherwise>
                    </xsl:choose>
                </cinematographerList>
                
                <!-- Get production company list -->
                <productionCompanyList>
                    <xsl:choose>
                        <xsl:when test="$getInfoBox//tr[contains(th,'Production')]/td/exists(//div[@class='plainlist'])">
                            <xsl:for-each select="$getInfoBox//tr[contains(th,'Production')]/td//div[@class='plainlist']/ul/li">
                                <productionCompany><xsl:value-of select="normalize-space(replace(.,'\[.*\]',''))"/></productionCompany>
                            </xsl:for-each>
                        </xsl:when>
                        <xsl:otherwise>
                            <productionCompany><xsl:value-of select="$getInfoBox//tr[contains(th,'Production')]/td"/></productionCompany>
                        </xsl:otherwise>
                    </xsl:choose>
                </productionCompanyList>
                
                
                <runningTime><xsl:value-of select="$getInfoBox//tr[contains(.,'Running')]/replace(td,'\[.*\]','')"/></runningTime>
                <budget><xsl:value-of select="$getInfoBox//tr[contains(.,'Budget')]/replace(replace(td,'\.|\$|\[.*\]',''),' million| billion','000000')"/></budget>
                <box><xsl:value-of select="$getInfoBox//tr[contains(.,'Box')]/replace(replace(td,'\.|\$|\[.*\]',''),' million| billion','000000')"/></box>
                
                <xsl:copy-of select="genres"></xsl:copy-of>
                <xsl:copy-of select="types"></xsl:copy-of>
                
                <plot>
                    <xsl:value-of select="doc($path)//p[preceding::span[@id='Plot'] and not(preceding::span[@id='Cast'])]"/>
                </plot>
            </film>
        </xsl:for-each>
        </data>
    </xsl:template>
    
    
</xsl:stylesheet>