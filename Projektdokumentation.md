# PROJEKTDOKUMENTATION "OSCAR-WINNING-FEATURES" (Arbeitstitel) #

**Aktuelle Projektdukomentation befindet sich in einem GoogleDoc!**

## Gruppentreffen vom 07.03.2019
##### Erkenntnisse (s. G-Doc) und nächste To-Dos wurden besprochen:

* Weitere Visualisierungen generieren (Netzwerk-Producer, Pie-Charts nach Jahren)
* Zusätzliche Funktionalitäten bei Visualisierungen programmieren (z.B. Auswahl von Filmen nach Jahren bei Netzwerken etc.)
* Visualisierungen integrieren
* Bugs bei Visualisierungen beseitigen (Zahlen sollen bei Bubble Chart nicht dargestellt werden)
* Lizenzen bzw. Links hinzufügen 
* Daten überprüfen ( N/A bei Visualisierungen etc.)
* Timeline verbessern (bessere Positionierung von Important events, evtl. farbliche Unterscheidung)


## Kurze Besprechung im Kurs am 15.1.2019 (Julia und Sviat)
##### 1.) Gewinnung des Datensatzes
Wir haben uns entschlossen, eine API zusätzlich zu den Daten aus der Wikipedia zu verwenden. Es handelt sich um eine API unter einer CC BY-NC 4.0
Lizenz.
* OMDb API
* http://www.omdbapi.com

## Arbeitstreffen mit Kurs am 8.1.2019

#### offene Fragen ####

##### 1.) Auswahl der Preise
* aktuelle Auswahl: Oscar, Goldene Palme, People's choice, Kassenschlager 
* Passt das so?

##### 2.) Umgang mit inkonsistenten Daten (in Datenauswahl und in Visualisierung)

* Wie gehen wir mit inkonsistenten Daten um?
* Wie gehen wir mit unvollständigen Daten um?
* Bsp.: Goldene Palme wurde ich manchen Jahren mehrfach vergeben, in manchen Jahren gar nicht
* Sammeln: Problemstellen

##### 3.) Auswahl für Features Genre und Gattung 
* Welche Genres und Features wählen wir aus?
* Liste aller Genres auf Wikipedia: 
 * Deutsch: https://de.wikipedia.org/wiki/Kategorie:Filmgenre
 * Englisch: https://en.wikipedia.org/wiki/Category:Films_by_genre
* andere Listen: http://www.film-genres.de/

Vorschlag Genres:
* Adventure Movie (...
* Action Movie (...
* Comedy (...
* Criminal
* Drama (Love Movie, ....
* Fantasy Movie (..., 
* Historical Film
* Horror Film
* Musikfilm?
* Science Fiction (...
* Western?
* Was ist mit Thriller?

Vorschlag Gattung (http://www.geschichte-projekte-hannover.de/filmundgeschichte/zitieren_und_dokumentieren/beschreibung_von_inhalt_und_form/filmgattungen_gestaltungsformen_und_genres.html)
* Dokumentarfilm
* Spielfilm
* Animationsfilm


##### 



## Gruppentreffen vom 11.12.2018
* Datenauswahl: Wir haben uns darauf geeinigt, nur Daten aus der englischsprachigen Wikipedia zu verwenden. Sie bietet den Vorteil, dass  1) mehr Daten (im Sinne von mehr verfügbaren und vollständigeren Texten) zu Verfügung stehen und 2) mehr Zusatzinformationen wie Metadaten oder Genres und Gruppierungen mitgeliefert werden.
* Organisation der Daten: Wie auch in der Wikipedia planen wir die Daten nach Genres und Gattungen zu kategorisieren.
* Visualisierung: Wir haben haben uns schon mit möglichen Problem in der Visualisierung beschäftigt. (Was machen wir bei inkonsistenten Daten? Was bei redundanten Daten? Wie soll die Visualisierungssoftware mit Daten umgehen, die mehr als einen Wert zurückgibt?)
* weiteres Features: Stimmung des Textes. Möglicherweise durch das Paket 'Vader' umsetzbar. [Beispiele](http://t-redactyl.io/blog/2017/04/using-vader-to-handle-sentiment-analysis-with-social-media-text.html)

## Gruppentreffen vom 4.12.2018 und vom 9.12.2018 ##

### Aktualisierung der Projektzusammenfassung in Ilias: Untersuchungsgegenstand und Ziel des Projektes 
[aktualisiert Dez. 2018: ]
* Das Projekt "Oscar-Winning Features" (Arbeitstitel, alternativ: Award-Winning Movie Features) ist der Erforschung von Merkmalen preisgekrönter Filme gewidmet. 
* Ziel des Projektes ist es, möglichst allgemeine Tendenzen für "Erfolgsmerkmale" prämierter Filme sowie versteckte Zusammenhänge zwischen unterschiedlichen Merkmalen (wie z. B. Titel, Erscheinungsjahr, Genre, Handlungsbeschreibung etc.) zu entdecken. 
* Dabei beschränken wir uns auf die Analyse bzw. Auswertung textueller Daten von Filmen, die im Rahmen unterschiedlicher Preisverleihungen und Ranglisten als "Bester Film" eines jeweiligen Jahres bezeichnet wurden (z. B. Oscar, Golden Globe oder Platz 1 der weltweit höchsten Einspielergebnisse eines Jahres). 
* Die Daten und Untersuchungsergebnisse werden auf einer Website visualisiert bzw. präsentiert.

### I. Ergebnisse ###

#### 1.) Anpassung des Untersuchungsgegenstandes und der Datengrundlage ####
* Statt einer Untersuchung der Features von Oscar-Prämiertern Filme aus verschiedenen Kategorien möchten wir nur die Kategorie "Bester Film" untersuchen. 
* Dafür möchten wir unsere Analyse auf andere internationale Awards bzw. Chartlisten ausweiten, die ebenso jährlichen einen besten Film küren. Passend wären: Oscar, Goldene Palme (Festival de Cannes), People's Choice Awards, Rangliste mit weltweiten Einspielergebnissen
* Hintergrund/Beweggrund: Dies ermöglicht es uns, Vergleiche zwischen Erfolgsmerkmalen unterschiedlicher Preise anzustellen. Die Beschränkung auf "Beste Filme" soll es uns zeitlich/logistisch ermöglichen, möglichst viele Features einzubeziehen. 

#### 2.) Konkretisierung Output: Was wollen wir auf unserer Website zeigen? ####
* Die Website soll es dem Nutzer/der Nutzerin zum einen ermöglichen, die verarbeiteten Daten und Statistiken über prämierte Filme flexibel zu visualisieren. Filtermöglichkeiten soll es dem Nutzer/der Nutzerin ermöglichen, unterschiedliche Features zu visualisieren und zu vergleichen. 
* Zum anderen möchten wir unsere Analyseergebnisse präsentieren. Dies möchten wir in Form von ... 
* *Siehe Mock-Up*

#### 3.a) Datenauswahl: Welche Awards/Ranglisten können wir nehmen? ####
* Welche internationalen Awards haben die Kategorie "Bester Film"? Oscar, Golden Globe (aber: Comedy und Drama getrennt), Goldene Palme
* Welche anderen Ranglisten können (analog zu Preisen) interessant sein? People's Choice Award, Liste finanziell erfolgreicher Filme
* *siehe Movie Data/awards_and_charts.md*

#### 3.b) Datenauswahl: Welche "internen" Features (Features von Filmen) können/möchten wir untersuchen? ####
Sammlung möglicher Features:
* Prize
* Category
* Title
* Year
* Country of Production
* Production Company
* Genre
* Gattung? 
* Budget
* Plot (as text from Wiki) >>> Topic modelling >>> Schlagwörter
* Length
* ...

*explizit KEINE nichttextlichen Infos wie Farben, Schnitthäufigkeit, Musik*

Die Verfügbarkeit verschiedener Daten von Filmen möchten wir erst anhand der Oscar-prämierten Filme testen

zu klären: Wie einzelne Features strukturieren bzw. verarbeiten? Z. B. Welche Genres sind erlaubt?

#### 3.c) Datenauswahl: Welche "externen Features" (Informationen außerhalb der Filme) oder Infos könnten wir einbeziehen? ####
* Idee: Themen sammeln, die in den jeweiligen Jahres gesellschaftlich relevant waren. Problem Datengrundlage: Was heißt gesellschaftlich relevant? Idee: Schlagwörter, die besonders häufig gegoogelt wurden. Problem dabei: Was ist mit den Jahren vor Google? Idee: Daten über Zeitungsartikel. Zu klären: Gibt es da ein Korpus oder eine Datenbank über Zeitungsartikel? Wäre es sinnvoll, z. B. Schlagwörter nur aus einer amerikanischen Zeitung zu nehmen? >>> kompliziert 
* alternative Idee: Darstellung von historischen Ereignissen aus den jeweiligen Kalenderjahren anhand eines Zeitstrahls. *Siehe timelines_of_history.md*
* Welche film-externen Daten sind außerdem schwer zu bekommen? Z. B. Daten über Oscar-"Jury"/wahlberechtigter Mitglieder, es gibt keine offiziellen Listen. Gibt es Studienergebnisse, die wir verwenden könnten?

### II) Weitere Vorgehensweise und To-Dos bis zum nächsten Gruppentreffen ###
* Ilias aktualisieren
* Erstmal anhand von Oscar die Datenverfügbarkeit checken, bevor wir weitere Filmdaten sammeln
* Genres auswählen/strukturieren/vereinfachen, z.B. Filmsite Genres
* Awards/Charts auswählen
* Frage: Was ist mit Preisen, die in bestimmten mehrfach vergeben wurden? Gewichten
* Website: Darstellung der Features im Filter
* Features auflisten
* Welche Visualierung je Feature? D3, GoogleChart
* später: Visualierung von verschiedenen Merkmalen? 